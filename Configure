#!/usr/bin/perl
#
# CFINGERD Configuration Script
#
# $Id$
#
# Copyright (c) 1996, Bitgate Software.
# Copyright (c) 1998-9, Infodrom Oldenburg, Martin Schulze <joey@infodrom.north.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 1, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

require 'perl/question.pl';
require 'perl/filemagic.pl';
require 'perl/gethost.pl';
require 'perl/generic.pl';

$VERSION = "1.4.2";
$| = 1;

$config{'config'} = "/etc/cfingerd.conf";

sub show_header {
	&disp_file("config/header");
}

sub show_footer {
	&disp_file("config/footer");
	if ($config{'os_type'} eq "FREEBSD") {
		print "*** NOTE: cfingerd is building using GNU make! On a BSD system\n";
		print "*** you need to install \"gmake\" before compiling cfingred.\n";
	}
}

sub make_userlist {
	print "Creating userlist/config.h ... ";

	open(CF, "userlist/config.h.in");
	open(CFO, ">userlist/config.h");
	while(<CF>) {
		print CFO;
	}

	if ($config{'os_type'} eq 'SUNOS') {
		print CFO $define_sunos;
	} elsif ($config{'make'} eq "gmake") {
		print CFO $define_bsd;
	}
	close(CFO);
	close(CF);

	print "created.\n";
}

sub make_config {
	print "Creating src/config.h ... ";

	$ENV{'LC_TIME'} = "C";
	local($dateandtime) = `date`;
	chop($dateandtime);

	open(CF, "src/config.h.in");
	open(CFO, ">src/config.h");
	while(<CF>) {
		chop;

		s/\@CFINGERD_CONF\@/#define\tCFINGERD_CONF\t\t\"$config{'config'}\"/;
		s/\@NOBODY_UID\@/#define\tNOBODY_UID\t\t$config{'nobody_uid'}/;
		s/\@NOBODY_GID\@/#define\tNOBODY_GID\t\t$config{'nobody_gid'}/;
		s/\@COMPILE_DT\@/#define\tCOMPILE_DT\t\t\"$dateandtime\"/;
		s/\@WTMPFILE\@/#define\tWTMPFILE\t\t"$config{'lastlog'}"/;
		s/\@MAIL_SPOOL_DIR\@/#define\tMAIL_SPOOL_DIR\t\t"$config{'mailspool'}"/;

		if (!$config{'lastlog'}) {
			s/\@USE_LASTLOG\@/#undef\tUSE_LASTLOG/;
		} else {
			s/\@USE_LASTLOG\@/#define\tUSE_LASTLOG\t\t1/;
		}

		if ($shadow eq "Y") {
			s/\@HAS_SHADOW\@/#define\tHAS_SHADOW\t\t1/;
		} else {
			s/\@HAS_SHADOW\@/#undef\tHAS_SHADOW/;
		}

		if (getgrnam("tty")) {
			s/\@HAS_TTY_GROUP\@/#define\tHAS_TTY_GROUP\t\t1/;
		} else {
			s/\@HAS_TTY_GROUP\@/#undef\tHAS_TTY_GROUP/;
		}

		print CFO "$_\n";
	}

	if ($config{'os_type'} eq 'SUNOS') {
		print CFO $define_sunos;
	} elsif ($config{'make'} eq "gmake") {
		print CFO $define_bsd;
	}
	close(CFO);
	close(CF);

	print "created.\n";
}

sub make_makefile {
	print "Creating Makefile.cfg ... ";

	open(CF, "Makefile.cfg.in");
	open(CFO, ">Makefile.cfg");

	while(<CF>) {
		chop;
		s/__OS_TYPE__/$config{'ostype'}/;
		s/__MAKE__/$config{'make'}/;
		s/__LDFLAGS__/$config{'ldflags'}/;
		s/__CFLAGS__/$config{'cflags'}/;
		s/__MANDIR__/$config{'mandir'}/;
		s/__MAN_OWNER__/$config{'man_owner'}/;
		s/__MAN_GROUP__/$config{'man_group'}/;
		s/__BIN_OWNER__/$config{'bin_owner'}/;
		s/__BIN_GROUP__/$config{'bin_group'}/;

		print CFO "$_\n";
	}

	close(CF);
	close(CFO);

	print "created.\n";
}

sub configure {
	unlink("src/config.h");

	$go_on = &check_exist("1", "./Makefile.cfg", "totally clean installation", "config/makewarn");
	if ($go_on && $go_on ne "Y") {
		&disp_file("config/cancelled");
		exit;
	}

	system("cp -p Makefile.in Makefile");
	($config{'os_type'},$os_ver) = &get_os;
	$os_ver =~ s/\.//g;
	$config{'lastlog'} = &get_lastlog ($config{'lastlog'});
	$nobody = &get_nobody ($config{'nobody_uid'},$config{'nobody_gid'});
	($config{'nobody_uid'},$config{'nobody_gid'}) = split(',', $nobody);
	$shadow = &has_shadow;
	$config{'mailspool'} = &get_mailspool ($config{'mailspool'});
	$config{'mandir'} = &get_mandir ($config{'mandir'});

	if ($config{'os_type'} eq "LINUX") {
		$config{'make'}    = "make" if (!exists $config{'make'});
		$config{'ldflags'} = "" if (!exists $config{'ldflags'});
		$config{'cflags'}  = "-g -O2 -Wall" if (!exists $config{'cflags'});
		$config{'bin_owner'} = "root" if (!exists $config{'bin_owner'});
		$config{'bin_group'} = "root" if (!exists $config{'bin_group'});
		$config{'man_owner'} = "man" if (!exists $config{'man_owner'});
		$config{'man_group'} = "man" if (!exists $config{'man_group'});
	} elsif ($config{'os_type'} eq "SUNOS") {
		$config{'make'}    = "gmake" if (!exists $config{'make'});
		$config{'ldflags'} = "-lsocket -lnsl" if (!exists $config{'ldflags'});
		$config{'cflags'}  = "" if (!exists $config{'cflags'});
		$config{'bin_owner'} = "bin" if (!exists $config{'bin_owner'});
		$config{'bin_group'} = "bin" if (!exists $config{'bin_group'});
		$config{'man_owner'} = "bin" if (!exists $config{'man_owner'});
		$config{'man_group'} = "bin" if (!exists $config{'man_group'});
	} elsif ($config{'os_type'} eq "FREEBSD") {
		$config{'make'}    = "gmake" if (!exists $config{'make'});
		$config{'ldflags'} = "" if (!exists $config{'ldflags'});
		$config{'cflags'}  = "" if (!exists $config{'cflags'});
		$config{'bin_owner'} = "root" if (!exists $config{'bin_owner'});
		$config{'bin_group'} = "wheel" if (!exists $config{'bin_group'});
		$config{'man_owner'} = "man" if (!exists $config{'man_owner'});
		$config{'man_group'} = "man" if (!exists $config{'man_group'});
	} else {
		$config{'make'}    = "gmake" if (!exists $config{'make'});
		$config{'ldflags'} = "" if (!exists $config{'ldflags'});
		$config{'cflags'}  = "" if (!exists $config{'cflags'});
		$config{'bin_owner'} = "root" if (!exists $config{'bin_owner'});
		$config{'bin_group'} = "root" if (!exists $config{'bin_group'});
		$config{'man_owner'} = "root" if (!exists $config{'man_owner'});
		$config{'man_group'} = "root" if (!exists $config{'man_group'});
	}

	print "\n";

	$define_bsd = "\n#ifndef BSD\n#define BSD 1\n#endif\n";
	$define_sunos = "\n#ifndef SUNOS\n#define SUNOS $os_ver\n#endif\n";

	&make_config;
	&make_userlist;
	&make_makefile;
}

# Parse arguments
$i = 0;
while ($i <= $#ARGV) {
    if ($ARGV[$i] eq "-h" || $ARGV[$i] eq "--help") {
	printf "./Configure [-c foo=bar] [-h]\n";
	foreach $foo (('make','cflags','ldflags','config','nobody_uid','nobody_gid','lastlog','mailspool','mandir')) {
	    printf "\t%s=\n", $foo;
	}
	exit 0;
    } elsif ($ARGV[$i] eq "-c") {
	if ($i+1 <= $#ARGV && ($ARGV[$i+1] =~ /(.+)=['"]?(.+)['"]?/)) {
	    $config{$1} = $2;
	    $i++;
	}
    } else {
	print STDERR "Unknown argument '$ARGV[$i]'.\n";
    }
    $i++;
}

&show_header;
&configure;
&show_footer;
