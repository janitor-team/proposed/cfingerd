# Makefile for a totally clean installation

all clean:
	@echo
	@echo "You need to type \"Configure\"."
	@echo
	@echo "Suggested reading:"
	@echo "  FAQ       - Gives some helpful information about CFINGERD"
	@echo "  README    - Gives information on how to install CFINGERD"
	@echo "  RECOMMEND - Gives suggestions on what to do after CFINGERD is installed"
	@echo
