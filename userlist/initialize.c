/*
 * USERLIST
 * Initialization routines
 */

#include "userlist.h"
#include "proto.h"
#include "config.h"
#ifdef	BSD
#include "../src/getutent.h"
#endif

#include <ctype.h>

TTY_FROM tty_list[MAX_TTYS];
int times_on;

char *remove_crap(char *str)
{
    char *ret;
    int i, x = 0;

    ret = (char *) malloc(strlen(str));
    for (i = 0; i < strlen(str); i++)
	if (isalnum(str[i]) || (str[i] == '.') || (str[i] == '-'))
	    ret[x++] = str[i];

    return(ret);
}

#ifdef BSD
#define ULIST_UNAME	16
#else
#define ULIST_UNAME	8
#endif
#define ULIST_TTY	2
#define ULIST_LOCALE	16
#define ULIST_LINE	12

void initialize_userlist(void)
{
    struct utmp *ut;
    char *cp;

    times_on = 0;

    while(((ut = getutent()) != NULL) && (times_on < MAX_TTYS))
#ifdef	BSD
	{
#else
	if (ut->ut_type == USER_PROCESS) {
#endif
	    tty_list[times_on].username = malloc(ULIST_UNAME+1);
	    tty_list[times_on].tty = malloc(ULIST_TTY+1);
	    tty_list[times_on].locale = malloc(ULIST_LOCALE+1);
	    tty_list[times_on].line = malloc(ULIST_LINE+1);
	    memset (tty_list[times_on].username, 0, ULIST_UNAME+1);
	    memset (tty_list[times_on].tty, 0, ULIST_TTY+1);
	    memset (tty_list[times_on].locale, 0, ULIST_LOCALE+1);
	    memset (tty_list[times_on].line, 0, ULIST_LINE+1);

#ifdef	BSD
	    strncpy(tty_list[times_on].username, (char *) ut->ut_name, ULIST_UNAME);
#else
	    strncpy(tty_list[times_on].username, (char *) ut->ut_user, ULIST_UNAME);
	    cp = (char *) ut->ut_line;
	    if (!strncmp(cp, "tty", 3)) cp+=3; /* strip ^tty */
	    strncpy(tty_list[times_on].tty, cp, ULIST_TTY);
#ifdef SUNOS
	    tty_list[times_on].ip_addr = 0;
#else /* SUNOS */
	    tty_list[times_on].ip_addr = ut->ut_addr;
#endif /* SUNOS */
#endif
	    cp = ut->ut_host;
	    if (*cp == ':')
		cp++;
	    strncpy(tty_list[times_on].locale, cp, ULIST_LOCALE);
	    if (strlen ((char *) ut->ut_host) > ULIST_LINE)
		if ((cp = strrchr(tty_list[times_on].locale, '.')) != NULL)
		    *cp = '\0';
	    strncpy(tty_list[times_on].line, (char *) ut->ut_line, ULIST_LINE);

	    tty_list[times_on].time = ut->ut_time;
	    times_on++;
	}
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  tab-width: 8
 * End:
 */
