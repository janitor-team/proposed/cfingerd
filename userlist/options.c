/*
 * USERLIST
 * Command line options
 */

#include "userlist.h"
#include "proto.h"

int display_type;
int no_idle = 0;

void give_help(void)
{
    printf("\nUSERLIST command line options\n\n");
    printf("A number of options selects the emulated OS display type for finger output.\n\n");
/*
    printf("\t-b\tGive BSD/OS-type finger output\n");
*/
    printf("\t-c\tGive standard CFINGERD (custom) output\n");
    printf("\t-n\tList only people idle less than one day\n");
/*
    printf("\t-g\tGive GNU-FINGER display output\n");
    printf("\t-l\tGive standard LINUX finger output (as if there is any)\n");
    printf("\t-r\tGive standard STRICT RFC1288 compilant output\n");
    printf("\t-u\tGive Unix System V finger output\n\n");
*/
    printf("Userlist for CFINGERD version 1.2.1\n\n");

    fflush(stdout);
    exit(1);
}

void handle_options(int argc, char *argv[])
{
    int i;

    for (i=1;i<argc;i++) {
	if (!strcmp(argv[i], "-c"))
	    display_type = DISPLAY_CFINGERD;
	else if (!strcmp(argv[i], "-n"))
	    no_idle = 1;
	else {
	    give_help();
	    return;
	}
    }
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  tab-width: 8
 * End:
 */
