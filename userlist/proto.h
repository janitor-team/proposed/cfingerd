/*
 * USERLIST
 * Prototype listing
 */

void initialize_userlist(void);
void process_display(void);
void handle_options(int, char *[]);
char *calc_idle(char *);
